from twisted.web.resource import Resource
from twisted.internet import reactor
from twisted.web.server import Site

from builtin import RESOURCES
from errors import MethodNotAllowed
from resources import  ResourceRegistry

import urlparse

registry = ResourceRegistry()


class ServerError(Exception):
    pass

class RootResource(Resource):
    isLeaf = True

    def render(self, request):
        api_path = '/'.join(request.postpath)
        print api_path

        # See if we have a matching resource
        try:
            matched_resource_class = registry[api_path]
            matched_resource = matched_resource_class()
        except KeyError:
            return '404 Not Found' #FIXME


        # Excute method
        try:
            response = matched_resource._render_request(request, request.method)
        except MethodNotAllowed as exp:
            return exp.generate_response(request)
        return response

rr = ResourceRegistry()
print rr._resources

resource = RootResource()

factory = Site(resource)
reactor.listenTCP(8080, factory)
reactor.run()



