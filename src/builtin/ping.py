from resources import Resource, api, schema 


@api('/ping')
class PingResource(Resource):
    '''
    A Ping resource that can be used to check if the 
    service is available.
    '''

    @schema(None, None)
    def read(self, request):
        '''
        Return a JSON object indicating the status of
        the service
        '''
        return {'Alive': True}
