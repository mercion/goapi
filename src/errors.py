import json

class ResourceException(Exception):
    code = 500
    def __init__(self, message=None, description=None, debug=None):
        if message is not None:
            self.message = message
        if description is not None:
            self.description = description
        if debug is not None:
            self.debug = debug

    def generate_response(self, request):
        response_body = {
            'code': self.code,
            'message': self.message,
            'description': self.description,
            'debug': self.debug}

        request.setResponseCode(
            self.code,
            self.message)
        return json.dumps(response_body)

class MethodNotAllowed(ResourceException):
    code = 405
    message = 'Method Not Allowed'
    description = 'Check RAML for allowed methods'
    debug = ''




