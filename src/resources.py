import json

from abc import ABCMeta, abstractmethod

from errors import MethodNotAllowed

METHOD_MAPPINGS = {
    'GET': 'read',
    'PUT': 'update',
    'POST':  'create',
    'DELETE': 'delete'}


class ResourceNotImplemented(NotImplementedError):
    def __init__(self, api_path):
        self.api_path = api_path

class ResourceBuildException(Exception):
    pass

def api(api_path):
    def wrapped(resource):
        resource._api_path = api_path
        return resource
    return wrapped

def schema(request_schema, response_schema):
    def wrapped(method):
        method._schema = (request_schema, response_schema)
        return method
    return wrapped

def resp_type(response_type):
    def wrapped(method):
        method._resp_type = response_type
        return method
    return  wrapped

def dummy_method_implementation(*args, **kwargs):
    raise ResoureNotImplementedError

RESOURCES = {}

class ResourceRegistryMeta(type):
    def __init__(cls, name, bases, nmspc):
        super(ResourceRegistryMeta, cls).__init__(name, bases, nmspc)
        if  not hasattr(cls, 'registry'):
            cls.registry = set()
        cls.registry.add(cls)
        cls.registry -= set(bases)
    def __iter__(cls):
        return iter(cls.registry)


class Resource(object):

    __metaclass__ = ResourceRegistryMeta
    __MAGIC_METHODS__ = ['create', 'read', 'update', 'delete']
    _resp_type = 'JSON'

    def __init__(self):
        for method_name in self.__MAGIC_METHODS__:
            method = getattr(self, method_name, None)
            if method and not hasattr(method, '_schema'):
                raise ResourceBuildException(
                    'Can not build resource {}. Method implementation '
                    '{} does not have a schema defined'.format(
                        self, method))

    def _render_request(self, request, method):
        func_name = METHOD_MAPPINGS[method]
        method = getattr(self, func_name, None)
        if method is None:
            raise MethodNotAllowed()
        response = method(request)
        if self._resp_type == 'JSON':
            return json.dumps(response)
        elif self._resp_type == 'XML':
            raise NotImplementedError()


class ResourceRegistry(Resource):
    __metaclass__ = ResourceRegistryMeta

    def __init__(self):
        self._resources = {}
        self.update_registry()

    def update_registry(self):
        self._resources = {}
        for resource in self.registry:
            if resource !=  self.__class__:
                print resource
                self._resources[resource._api_path] = resource
    def __getitem__(self,  api_path):
        return self._resources[api_path]


if __name__ == "__main__":
    @api('/fruits')
    class FruitResource(Resource):
        @schema(None, None)
        def create(self, request):
            pass

    print FruitResource._api_path
    F = FruitResource()


    rr = ResourceRegistry()

    print '-'*80
    print rr._resources
